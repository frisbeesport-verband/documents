# DFV Document Styles

## Usage

### Linux
Custom styles can be put into `~/texmf/tex/latex/local` where the compiler will look for them.

* **Option 1**: Clone the repository and create a symlink to the `texmf` folder
  *  `ln -s <Path to cloned Repository>/styles ~/texmf/tex/latex/local/dfv`
  * *Note*: For some strange reason, the styles folder needs to contain at least one folder that is not a symlink `mkdir symlink-fix`
* **Option 2**: Download the styles folder and copy it manually to your `texmf` folder.
